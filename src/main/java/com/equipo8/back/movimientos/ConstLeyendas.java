package com.equipo8.back.movimientos;

public enum ConstLeyendas {
    PAGO_CUENTA_TERCERO("Movimiento BBVA"),
    SPEI_ENVIADO("Transferencia interbancaria enviada"),
    SPEI_RECIBIDO("Transferencia interbancaria recibida"),
    RETIRO_SIN_TARJETA("Retiro Sin Tarjeta"),
    RECARGAS("Recargas y Paquetes");

    private String value;

    private ConstLeyendas(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }
}

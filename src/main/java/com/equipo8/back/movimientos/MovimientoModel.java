package com.equipo8.back.movimientos;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "movimientos")
public class MovimientoModel {
    private String idmovto;
    private String cuenta;
    private String fecha;
    private String monto;
    private String codigo_ley;
    private String desc_ley;


    public MovimientoModel() {
    }

    public MovimientoModel(String idmovto, String cuenta, String fecha, String monto, String codigo_ley, String desc_ley) {
        this.idmovto = idmovto;
        this.cuenta = cuenta;
        this.fecha = fecha;
        this.monto = monto;
        this.codigo_ley = codigo_ley;
        this.desc_ley = desc_ley;
    }

    public String getIdmovto() {
        return idmovto;
    }

    public void setIdmovto(String idmovto) {
        this.idmovto = idmovto;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getCodigo_ley() {
        return codigo_ley;
    }

    public void setCodigo_ley(String codigo_ley) {
        this.codigo_ley = codigo_ley;
    }

    public String getDesc_ley() {
        return desc_ley;
    }

    public void setDesc_ley(String desc_ley) {
        this.desc_ley = desc_ley;
    }

}

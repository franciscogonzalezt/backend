package com.equipo8.back.movimientos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovimientoService {
   @Autowired
    MovimientoRepository movimientoRepository;

   public List<MovimientoModel> findAllMovimientos() {
       return movimientoRepository.findAll();
   }

   public List<MovimientoModel> findAllMovimientosCuenta(String cuenta) {
       return movimientoRepository.findByCuenta(cuenta);
   }
   public List<MovimientoModel> findallMovimientosFecha(String fecha){
       return movimientoRepository.findByFecha(fecha);
   }

   public MovimientoModel addMovimiento(MovimientoModel movimiento) {
       String leyenda = movimiento.getCodigo_ley();

       if (leyenda.equalsIgnoreCase("pago cuenta de tercero")) {
           movimiento.setDesc_ley(ConstLeyendas.PAGO_CUENTA_TERCERO.getValue());
       } else if (leyenda.toLowerCase().contains("spei enviado")) {
           movimiento.setDesc_ley(ConstLeyendas.SPEI_ENVIADO.getValue());
       } else if (leyenda.toLowerCase().contains("spei recibido")) {
           movimiento.setDesc_ley(ConstLeyendas.SPEI_RECIBIDO.getValue());
       } else if (leyenda.equalsIgnoreCase("retiro sin tarjeta")) {
           movimiento.setDesc_ley(ConstLeyendas.RETIRO_SIN_TARJETA.getValue());
       } else if (leyenda.equalsIgnoreCase("recargas y paquetes bmov")) {
           movimiento.setDesc_ley(ConstLeyendas.RECARGAS.getValue());
       }

       return movimientoRepository.save(movimiento);
   }
}
package com.equipo8.back.bancos;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bancos")
public class BancoModel {

    String banco;
    String clave;

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}

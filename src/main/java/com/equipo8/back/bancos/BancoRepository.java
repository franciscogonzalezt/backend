package com.equipo8.back.bancos;

import com.equipo8.back.detalles.DetalleModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BancoRepository extends MongoRepository<BancoModel, String> {
    @Query(value = "{banco : ?0}")
    public List<BancoModel> findByNomBanco(String banco);

    @Query("{clave : ?0}")
    public List<BancoModel> findByClaveBanco(String clave);

}

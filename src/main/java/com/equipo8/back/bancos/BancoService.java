package com.equipo8.back.bancos;

import com.equipo8.back.detalles.DetalleModel;
import com.equipo8.back.detalles.DetalleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BancoService {
    @Autowired
    BancoRepository bancoRepository;

    public List<BancoModel> findByNomBanco(String banco) {
        return bancoRepository.findByNomBanco(banco);
    }
    public List<BancoModel> findBancoById(String clave) {
        return bancoRepository.findByClaveBanco(clave);
    }
}
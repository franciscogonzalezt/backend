package com.equipo8.back.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
public class Configuracion {

    private String url;
    private String serie;
    private String banco;
    private String llave;

    public Configuracion() {
    }

    public String getUrl() {
        return url;
    }

    public String getSerie() {
        return serie;
    }

    public String getBanco() {
        return banco;
    }

    public String getLlave() {
        return llave;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }


}

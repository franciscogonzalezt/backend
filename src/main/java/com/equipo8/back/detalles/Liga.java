package com.equipo8.back.detalles;


import com.equipo8.back.bancos.BancoModel;
import com.equipo8.back.bancos.BancoRepository;
import com.equipo8.back.bancos.BancoService;
import com.equipo8.back.encripta.AESSanitize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Liga {

    private String fecha;
    private String rastreo;
    private String banco_ord;
    private String banco_ben;
    private String cuenta;
    private String monto;
/*
    public Liga() {
    }

 */
/*
    public Liga(String fecha, String rastreo, String banco_ord, String banco_ben, String cuenta, String monto) {

        this.fecha = fecha;
        this.rastreo = rastreo;
        this.banco_ord = banco_ord;
        this.banco_ben = banco_ben;
        this.cuenta = cuenta;
        this.monto = monto;
    }
*/
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRastreo() {
        return rastreo;
    }

    public void setRastreo(String rastreo) {
        this.rastreo = rastreo;
    }

    public String getBanco_ord() {
        return banco_ord;
    }

    public void setBanco_ord(String banco_ord) {
        this.banco_ord = banco_ord;
    }

    public String getBanco_ben() {
        return banco_ben;
    }

    public void setBanco_ben(String banco_ben) {
        this.banco_ben = banco_ben;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getCadenaOri(String clave_ban_ord, String clave_ban_ben){

        return fecha +  "|" + "T" + "|" + rastreo + "|" + clave_ban_ord + "|" + clave_ban_ben + "|" + monto;
    }
}

package com.equipo8.back.detalles;

import com.equipo8.back.movimientos.MovimientoModel;
import com.equipo8.back.movimientos.MovimientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DetalleService {
    @Autowired
    DetalleRepository detalleRepository;

    public List<DetalleModel> findDetalle(String idmovto) {
        return detalleRepository.findByIdmovto(idmovto);
    }

}
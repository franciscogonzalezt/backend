package com.equipo8.back.detalles;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "detalles")
public class DetalleModel {

    private String idmovto;
    private String cuenta;
    private String fecha;
    private String monto;
    private String nom_ord;
    private String ban_ord;
    private String nom_ben;
    private String ban_dest;
    private String cuenta_ben;
    private String fecha_apli;
    private String referencia;
    private String rastreo;
    private String folio;
    private String canal;

    public DetalleModel() {
    }

    public DetalleModel(String idmovto, String cuenta, String fecha, String monto, String nom_ord, String ban_ord, String nom_ben, String ban_dest, String cuenta_ben, String fecha_apli, String referencia, String rastreo, String folio, String canal) {
        this.idmovto = idmovto;
        this.cuenta = cuenta;
        this.fecha = fecha;
        this.monto = monto;
        this.nom_ord = nom_ord;
        this.ban_ord = ban_ord;
        this.nom_ben = nom_ben;
        this.ban_dest = ban_dest;
        this.cuenta_ben = cuenta_ben;
        this.fecha_apli = fecha_apli;
        this.referencia = referencia;
        this.rastreo = rastreo;
        this.folio = folio;
        this.canal = canal;
    }

    public String getIdmovto() {
        return idmovto;
    }

    public void setIdmovto(String idmovto) {
        this.idmovto = idmovto;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getNom_ord() {
        return nom_ord;
    }

    public void setNom_ord(String nom_ord) {
        this.nom_ord = nom_ord;
    }

    public String getBan_ord() {
        return ban_ord;
    }

    public void setBan_ord(String ban_ord) {
        this.ban_ord = ban_ord;
    }

    public String getNom_ben() {
        return nom_ben;
    }

    public void setNom_ben(String nom_ben) {
        this.nom_ben = nom_ben;
    }

    public String getBan_dest() {
        return ban_dest;
    }

    public void setBan_dest(String ban_dest) {
        this.ban_dest = ban_dest;
    }

    public String getCuenta_ben() {
        return cuenta_ben;
    }

    public void setCuenta_ben(String cuenta_ben) {
        this.cuenta_ben = cuenta_ben;
    }

    public String getFecha_apli() {
        return fecha_apli;
    }

    public void setFecha_apli(String fecha_apli) {
        this.fecha_apli = fecha_apli;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getRastreo() {
        return rastreo;
    }

    public void setRastreo(String rastreo) {
        this.rastreo = rastreo;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }
}

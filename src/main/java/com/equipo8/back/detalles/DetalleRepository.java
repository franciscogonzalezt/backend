package com.equipo8.back.detalles;

import com.equipo8.back.movimientos.MovimientoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;





@Repository
public interface DetalleRepository extends MongoRepository<DetalleModel, String> {
    @Query("{idmovto : ?0}")
    public List<DetalleModel> findByIdmovto(String idmovto);


}
package com.equipo8.back.detalles;

public class LigaConstruida {

    String url;

    public LigaConstruida(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

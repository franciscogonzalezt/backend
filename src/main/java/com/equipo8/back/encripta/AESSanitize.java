package com.equipo8.back.encripta;

import org.springframework.stereotype.Component;

@Component
public class AESSanitize {

    public String sanitizaCadena(String sCadenaE){
        String sCadenaT = sCadenaE.replaceAll("=", "%3D");
        sCadenaT = sCadenaT.replaceAll("/", "%2F");
        sCadenaT = sCadenaT.replaceAll("\\+", "%2B");
        return sCadenaT;
    }
}

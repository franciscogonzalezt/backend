package com.equipo8.back.encripta;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class AESExecute {

	/**
	 * @param args
	 */
	  static String IV = "FISANOSREPAERDNA";
	  static String plaintext = "test text 123\0\0\0"; 
	  static String encryptionKey = "MOC.REMOCNABAVBB";
	  
	public static void main(String[] args) {
		
		try {
///*  
		      System.out.println("plain:   " + plaintext);
		      
		      byte[] cipher = encryptLocal(plaintext);
		      String sKeyN = new String(Base64.encodeBase64(cipher));
		      System.out.println("cipher:  " + sKeyN );
		      String decrypted = decryptLocal(Base64.decodeBase64(sKeyN.getBytes()));
		      System.out.println("decrypt: " + decrypted);
//*/
		    } catch (Exception e) {
		      e.printStackTrace();
		    } 
	}


	  public static byte[] encryptLocal(String plainText) throws Exception {
		    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
		    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		    cipher.init(Cipher.ENCRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
		    return cipher.doFinal(plainText.getBytes("UTF-8"));
		  }

		  public static String decryptLocal(byte[] cipherText) throws Exception{
			 
		    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
		    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		    cipher.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
		    return new String(cipher.doFinal(cipherText),"UTF-8");
		  }
		  
	
	 public static  String encrypt(String clearText, String key) throws Exception {
		 System.out.println("ENTRA: ENCRYPT: " + Base64.encodeBase64( AESEncrypt.encryptBase64EncodedWithManagedIV(clearText, key)));
		 return new String(Base64.encodeBase64( AESEncrypt.encryptBase64EncodedWithManagedIV(clearText, key)));
		 
	    }
	  
	  public static String decrypt(String encryptedText, String key) throws Exception {
	        return new String(AESDecrypt.dencryptBase64EncodedWithManagedIV(encryptedText, key));
	    }
}

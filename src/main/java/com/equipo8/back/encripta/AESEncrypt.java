package com.equipo8.back.encripta;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AESEncrypt {

    private static final String cipherTransformation = "AES/CBC/PKCS5Padding";
    private static final String aesEncryptionAlgorithm = "AES";
 
    public static  byte[] encryptBase64EncodedWithManagedIV(String encryptedText, String key) throws Exception {
    
        byte[] cipherText =encryptedText.getBytes();
        byte[] keyBytes = Base64.decodeBase64(key.getBytes());
        return encryptWithManagedIV(cipherText, keyBytes);
       // return null;
    }

   public static  byte[] encryptWithManagedIV(byte[] cipherText, byte[] key) throws Exception{
    	

      /*  byte[] key16 = Arrays.copyOfRange(key,0,16);
        byte[] initialVector = Arrays.copyOfRange(key,16,key.length);*/
        byte[] key16Metodo = copiaRango(key,0,16);
        byte[] initialVectorMetodo=copiaRango(key,16,key.length);
      
        return encrypt(cipherText, key16Metodo, initialVectorMetodo);
     
    }

    public static byte[] encrypt(byte[] cipherText, byte[] key, byte[] initialVector) throws Exception{
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpecy = new SecretKeySpec(key, aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpecy, ivParameterSpec);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }
    public static byte[] copiaRango(byte[] arr, int desde, int hasta){
      	 int tamano = hasta-desde;
      	 if(tamano==0)
      	 tamano=1;
            int j=0;
    byte[] arrResultado = new byte[tamano];
    for(int i=desde; i<hasta;i++){
    arrResultado[j]=arr[i];
   // System.out.println("arr:"+arrResultado[j]);
    j++;
    }
    return arrResultado;
   }
    
}

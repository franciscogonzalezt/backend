package com.equipo8.back.encripta;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AESDecrypt {

    private static final String cipherTransformation = "AES/CBC/PKCS5Padding";
    private static final String aesEncryptionAlgorithm = "AES";
    
  public static byte[] dencryptBase64EncodedWithManagedIV(String encryptedText, String key) throws Exception {
        byte[] cipherText =Base64.decodeBase64(encryptedText.getBytes());
        byte[] keyBytes = Base64.decodeBase64(key.getBytes());
        return dencryptWithManagedIV(cipherText, keyBytes);
      
    }

   public static byte[] dencryptWithManagedIV(byte[] cipherText, byte[] key) throws Exception{
    	 
          
       // byte[] key16 = Arrays.copyOfRange(key,0,16);
        //byte[] initialVector = Arrays.copyOfRange(key,16,key.length); 
      
	   byte[] key16Metodo = copiaRango(key,0,16);
       byte[] initialVectorMetodo=copiaRango(key,16,key.length);
     
              
        return dencrypt(cipherText, key16Metodo, initialVectorMetodo);
    }

    public static byte[] dencrypt(byte[] cipherText, byte[] key, byte[] initialVector) throws Exception{
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpecy = new SecretKeySpec(key, aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }
    
    public byte[] copiaCadenaRango(byte[] oldArray, int newSize){
    	 byte[] key16 =null;
    	 
    	 int oldSize = java.lang.reflect.Array.getLength(oldArray);
    	 Class elementType = oldArray.getClass().getComponentType();
    	 Object newArray = java.lang.reflect.Array.newInstance(
    	 elementType,newSize);
    	 int preserveLength = Math.min(oldSize,newSize);
    	 if (preserveLength > 0)
    	 System.arraycopy (oldArray,0,newArray,0,preserveLength);

    	 return (byte[]) newArray; 
    	 
    		 
    		 } 
    	 
    public static  byte[] copiaRango(byte[] arr, int desde, int hasta){
    	int tamaniArreglo=hasta-desde;
    	boolean validacion=true;
        byte[] arrResultado = new byte[tamaniArreglo];
        int i=0;
    	try{
    	 for(int j=0; j<arr.length; j++){
    		 validacion=j>=desde&&j<hasta;
    		 if(validacion){
		        	arrResultado[i]=arr[j];
		       	i++;
		        }
		      }
        }catch(Exception e){
        	System.out.println("ha sucedido un error: "+e.getMessage());
        	
        }
           
        return arrResultado;
}    

}

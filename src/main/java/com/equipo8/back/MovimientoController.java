package com.equipo8.back;


import com.equipo8.back.bancos.BancoModel;
import com.equipo8.back.bancos.BancoService;
import com.equipo8.back.detalles.LigaConstruida;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.equipo8.back.detalles.DetalleModel;
import com.equipo8.back.detalles.DetalleService;
import com.equipo8.back.detalles.Liga;
import com.equipo8.back.encripta.AESExecute;
import com.equipo8.back.encripta.AESSanitize;
import com.equipo8.back.movimientos.MovimientoModel;
import com.equipo8.back.movimientos.MovimientoService;
import com.equipo8.back.utils.Configuracion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping(path = "/api/v01")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST})
public class MovimientoController {

    Logger logger = LoggerFactory.getLogger(AESSanitize.class);
    @Autowired
    MovimientoService movimientoService;
    @Autowired
    DetalleService detalleService;
    @Autowired
    AESSanitize sanitize;
    @Autowired
    Configuracion config;
    @Autowired
    BancoService bancoService;

    @GetMapping(path = "/movimientos/")
    public ResponseEntity<Object> getMovimientos() {
        List<MovimientoModel> list = movimientoService.findAllMovimientos();
        if (list.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(list);
        }
    }

    @GetMapping(path = "/movimientos/cuenta/{cuenta}")
    public ResponseEntity<Object> getMovimientosCuenta(@PathVariable String cuenta) {
        List<MovimientoModel> list = movimientoService.findAllMovimientosCuenta(cuenta);
        if (list.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(list);
        }
    }

    @GetMapping(path = "/movimientos/fecha/{fecha}")
    public ResponseEntity<Object> getMovimientosFecha(@PathVariable String fecha) {
        List<MovimientoModel> list = movimientoService.findallMovimientosFecha(fecha);
        if (list.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(list);
        }
    }

    @GetMapping(path = "/movimientos/{idmovto}")
    public ResponseEntity<Object> getDetalleMovimiento(@PathVariable String idmovto){
        List<DetalleModel> list = detalleService.findDetalle(idmovto);
        if (list.isEmpty()) {
            return ResponseEntity.notFound().build();
        }   else {
            return ResponseEntity.ok().body(list);
        }
    }

    @PostMapping(path = "/movimientos/getliga")
    public ResponseEntity<Object> getConfig(@RequestBody Liga liga) {

        String sCadenaE = "";
        try {
            String clave_ban_ord = "";
            String clave_ban_ben = "";
            List<BancoModel> banco = bancoService.findByNomBanco(liga.getBanco_ben().toLowerCase());
            clave_ban_ben = banco.get(0).getClave();
            banco.clear();
            banco = bancoService.findByNomBanco(liga.getBanco_ord().toLowerCase());
            clave_ban_ord = banco.get(0).getClave();


            sCadenaE = AESExecute.encrypt(liga.getCadenaOri(clave_ban_ord,clave_ban_ben), config.getLlave());
            sCadenaE = sanitize.sanitizaCadena(sCadenaE);

        } catch (Exception e) {
            System.out.println(e);
        }
        String sURL = config.getUrl() + "?i=" + config.getBanco() + "&s=" + config.getSerie() + "&d=" + sCadenaE;
        LigaConstruida url = new LigaConstruida(sURL);


        return ResponseEntity.ok().body(url);
    }

    @PostMapping(path = "/movimientos/")
    public ResponseEntity<Object> addMovimiento(@RequestBody MovimientoModel movimiento) {
        MovimientoModel mov = movimientoService.addMovimiento(movimiento);
        return ResponseEntity.ok().body(mov);
    }
}

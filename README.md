# Rest Empleados

## Descripcion

Backend Equipo 8

API REST con integración de métodos GET, POST y control de respuestas HTTP.

## Ejecucion

### Compile and Start

```
mvn clean package spring-boot:start
```

### Con ayuda de IntelliJ

```
En el menu de Maven:
    1. Lifecycle -> Package
    2. spring-boot -> spring-boot:stop
    2. spring-boot -> spring-boot:start
```

## Ejemplo de Uso
Para probar con Postman:

### GET

URL:
```
http://localhost:8080/api/v01/movimientos/
```

Ejemplo de Respuesta:

```json
[
  {
    "idmovto": "1",
    "cuenta": "123456789012345678",
    "fecha": "2021-04-22",
    "monto": "200.22",
    "codigo_ley": "Spei enviado Banamex",
    "desc_ley": "Transferencia interbancaria enviada"
  }
]
```